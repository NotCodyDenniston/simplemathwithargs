package com.example.simplemathwithargs

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SimpleMathApplication : Application()
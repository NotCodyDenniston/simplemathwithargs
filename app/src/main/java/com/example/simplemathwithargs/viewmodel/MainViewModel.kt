package com.example.simplemathwithargs.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.simplemathwithargs.model.MathRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

@HiltViewModel
class MainViewModel @Inject constructor(private val repo: MathRepo) : ViewModel() {

    private val _equation: MutableStateFlow<Double> = MutableStateFlow(0.0)
    val equationState: StateFlow<Double> get() = _equation

    fun EvaluateExpression(expr:String) = viewModelScope.launch{
        _equation.value = repo.EvaluateExpression(expr)
    }
}
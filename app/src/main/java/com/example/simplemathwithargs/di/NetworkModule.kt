package com.example.simplemathwithargs.di

import com.example.simplemathwithargs.model.remote.MathService
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import retrofit2.Retrofit
import retrofit2.create

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @OptIn(ExperimentalSerializationApi::class)
    @Provides
    fun providesRetrofit(): Retrofit {
        val base_Url = "http://api.mathjs.org/"
        val version = "v4/"
        val contentType: MediaType = MediaType.get("application/json")
        return Retrofit.Builder()
            .baseUrl(base_Url+version)
            .addConverterFactory(Json.asConverterFactory(contentType))
            .build()
    }

    @Provides
    fun providesRetrofitService(retrofit: Retrofit): MathService = retrofit.create()
}
package com.example.simplemathwithargs.model

import com.example.simplemathwithargs.model.remote.MathService
import javax.inject.Inject

class MathRepo @Inject constructor(
    private val service: MathService
) {

    suspend fun EvaluateExpression(expr:String): Double{
        return service.EvaluateExpression(expr)
    }
}
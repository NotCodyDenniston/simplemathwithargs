package com.example.simplemathwithargs.view.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Text
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.simplemathwithargs.R
import com.example.simplemathwithargs.ui.theme.SimpleMathWithArgsTheme

class OperatorSelector : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val args:String = arguments?.getString("firstNum") ?: "0.0"
                val operatorValue = remember { mutableStateOf("") }
                val expanded = remember { mutableStateOf(false) }
                val operators = listOf("+", "-", "*", "/")
                SimpleMathWithArgsTheme() {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        Text(text = "Operator")
                        Text(text = operatorValue.value)
                        Button(onClick = {
                            operatorValue.value =  args + operatorValue.value
                            val opArgs = bundleOf("Operator Arg" to operatorValue.value)
                            findNavController().navigate(R.id.second_input, opArgs)
                        }) {
                            Text(text = "enter")
                        }
                        Column(
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.Center) {
                            Button(onClick = { expanded.value = true }) {
                                Text(text = "Operators")
                            }
                        DropdownMenu(
                            modifier = Modifier.size(100.dp),
                            expanded = expanded.value,
                            onDismissRequest =  {expanded.value = false },
                        ) {
                            operators.forEach { operator ->
                                DropdownMenuItem(onClick = { operatorValue.value = operator }, text = { Text(
                                    text = operator
                                ) })
                        }
                    }
                        }
                    }
                }
            }
        }

    }
}
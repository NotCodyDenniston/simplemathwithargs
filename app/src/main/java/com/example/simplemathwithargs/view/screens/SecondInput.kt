package com.example.simplemathwithargs.view.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.input.KeyboardType
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.simplemathwithargs.R
import com.example.simplemathwithargs.ui.theme.SimpleMathWithArgsTheme
import com.example.simplemathwithargs.viewmodel.MainViewModel

class SecondInput: Fragment() {
    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val viewModel: MainViewModel by activityViewModels()
                val args:String = arguments?.getString("Operator Arg") ?: "0.0"
                val secondValue = remember { mutableStateOf("") }
                SimpleMathWithArgsTheme() {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        Text(text = "Second Value")
                        TextField(
                            value = secondValue.value,
                            onValueChange = { secondValue.value = it },
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
                        )
                        Button(onClick = {
                            secondValue.value = args + secondValue.value
                            viewModel.EvaluateExpression(secondValue.value)
                            findNavController().navigate(R.id.result) } ) {
                            Text(text = "enter")
                        }
                    }
                }
            }
        }

    }
}
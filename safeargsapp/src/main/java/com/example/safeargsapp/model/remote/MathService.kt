package com.example.simplemathwithargs.model.remote

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MathService {

    @GET(".")
    suspend fun EvaluateExpression(@Query("expr", encoded = false)expr : String) : Double
}
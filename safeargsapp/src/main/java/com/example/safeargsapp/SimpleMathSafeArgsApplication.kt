package com.example.safeargsapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SimpleMathSafeArgsApplication : Application()
package com.example.safeargsapp.view.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Text
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.safeargsapp.R
import com.example.safeargsapp.ui.theme.SimpleMathWithArgsTheme

class OperatorSelector : Fragment() {
    private val args: OperatorSelectorArgs by navArgs()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val operatorValue = remember { mutableStateOf("") }
                val expanded = remember { mutableStateOf(false) }
                val operators = listOf("+", "-", "*", "/")
                SimpleMathWithArgsTheme() {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        Text(text = "Operator")
                        Text(text = operatorValue.value)
                        Button(onClick = {

                            operatorValue.value = args.firstValue +  operatorValue.value
                            val action = OperatorSelectorDirections.goToSecond(operatorValue.value)
                            findNavController().navigate(action)
                        }) {
                            Text(text = "enter")
                        }
                        Column(
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.Center
                        ) {
                            Button(onClick = { expanded.value = true }) {
                                Text(text = "Operators")
                            }
                            DropdownMenu(
                                modifier = Modifier.size(100.dp),
                                expanded = expanded.value,
                                onDismissRequest = { expanded.value = false },
                            ) {
                                operators.forEach { operator ->
                                    DropdownMenuItem(
                                        onClick = { operatorValue.value = operator },
                                        text = {
                                            Text(
                                                text = operator
                                            )
                                        })
                                }
                            }
                        }
                    }
                }
            }
        }

    }
}
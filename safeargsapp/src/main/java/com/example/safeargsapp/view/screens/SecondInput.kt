package com.example.safeargsapp.view.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.input.KeyboardType
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavArgs
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.safeargsapp.R
import com.example.safeargsapp.ui.theme.SimpleMathWithArgsTheme
import com.example.simplemathwithargs.viewmodel.MainViewModel

class SecondInput: Fragment() {
    private val args: SecondInputArgs by navArgs()

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val viewModel: MainViewModel by activityViewModels()
                val secondValue = remember { mutableStateOf("") }
                SimpleMathWithArgsTheme() {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        Text(text = "Second Value")
                        TextField(
                            value = secondValue.value,
                            onValueChange = { secondValue.value = it },
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
                        )
                        Button(onClick = {
                            secondValue.value = args.operatorValue + secondValue.value
                            viewModel.EvaluateExpression(secondValue.value)
                            val action = SecondInputDirections.goToResult()
                            findNavController().navigate(action) } ) {
                            Text(text = "enter")
                        }
                            Text(text = "${secondValue.value}")
                    }
                }
            }
        }

    }
}